
export default {
    dateToTime(date) {
        return date ? this.ISOTimestampToLocalTima(date) : '00:00:00'
    },
    minutesSinceDate(date) {
        if (date) {
            const date_now = new Date
            const utc_now = date_now.getTime() + (date_now.getTimezoneOffset() * 1000 * 60)
            return ((utc_now - date) / (1000 * 60)).toFixed(0)
        }
        return -1
    },
    ISOTimestampToLocalTima(timestamp) {
        if (!timestamp) return
        let timeStr
        timeStr = timestamp.substr(11, 8)
        let hours = parseInt(timeStr.substr(0, 2))
        hours += 2
        hours = hours > 23 ? 0 + hours - 24 : hours
        timeStr = timeStr.replace(timeStr.substr(0, 2), hours)
        return timeStr
    },
    todayDateStr() {
        const date = new Date()
        const dd = String(date.getUTCDate()).padStart(2, '0')
        const mm = String(date.getUTCMonth() + 1).padStart(2, '0')
        const yyyy = date.getUTCFullYear()

        const date_str = dd + '-' + mm + '-' + yyyy
        return date_str
    },
    utcISOStringForHour(hour) {
        const date = new Date()
        const dd = String(date.getUTCDate()).padStart(2, '0')
        const mm = String(date.getUTCMonth()+1).padStart(2, '0')
        const yyyy = date.getUTCFullYear()
        return `${yyyy}-${mm}-${dd}T${String(hour).padStart(2, '0')}:00:00.000Z`
    },
    utcTimeNow() {
        const date = new Date()
        return `${date.getUTCHours()}:${date.getUTCMinutes()}`
    }
}
