import Vue from 'vue'
import App from './App'
import router from './router'
import store from './store'
import firebase from "./services/firebase"
import './services/fontawesome'

Vue.config.productionTip = false

let app
firebase.auth().onAuthStateChanged(async user => {
    await store.dispatch('fetchUser', user)
    if (!app) {
        app = new Vue({
            router,
            store,
            render: h => h(App)
        }).$mount('#app')
    }
})
