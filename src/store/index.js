import Vue from 'vue'
import Vuex from 'vuex'
import { vuexfireMutations, firebaseAction } from 'vuexfire'
import firebase from '../services/firebase'

import Bot from '../models/Bot'
import Filter from '../models/Filter'

Vue.use(Vuex)

// const bubbleSort = function (a, par, descent) {
//     let swapped
//     do {
//         swapped = false
//         for (let i = 0; i < a.length - 1; i++) {
//             if ((a[i][par] < a[i + 1][par] && descent) || (a[i][par] > a[i + 1][par] && !descent)) {
//                 const temp = a[i]
//                 a[i] = a[i + 1]
//                 a[i + 1] = temp
//                 swapped = true
//             }
//         }
//     } while (swapped)
// }

export default new Vuex.Store({
    state: {
        user: {
            loggedIn: false,
            data: null
        },
        config: {},
        bots: [],
        currentBot: {},
        filters: [],
        currentFilter: {},
        hiddenStatsIndexes: [1, 2, 3, 4, 5],
    },
    getters: {
        user(state) {
            return state.user
        },
        bots(state) {
            let bots = state.bots.map(botData => { return new Bot(botData) })
            // if (state.sortType === 'ID') {
            //     bubbleSort(bots, 'id', false)
            // } else {
            //     const sortedBots = bots.filter(bot => !state.hiddenStatsIndexes[bot.id])
            //     const hiddenStatsIndexes = []
            //     Object.keys(state.hiddenStatsIndexes).forEach(key => hiddenStatsIndexes.push(state.hiddenStatsIndexes[key]))
            //     bubbleSort(hiddenStatsIndexes, 'total', false)
            //     hiddenStatsIndexes.forEach(botStats => {
            //         sortedBots.push(bots.filter(bot => bot.id === botStats.botId))
            //     })
            //     bots = sortedBots
            // }

            return bots
        },
        currentBot(state) {
            return new Bot(state.currentBot)
        },
        filters(state) {
            return state.filters.map(filterData => { return new Filter(filterData) })
        },
        currentFilter(state) {
            return new Filter(state.currentFilter)
        },
        hiddenStatsIndexes(state) {
            return state.hiddenStatsIndexes
        }
    },
    mutations: {
        ...vuexfireMutations,
        setLoggedIn(state, value) {
            state.user.loggedIn = value
        },
        setUser(state, data) {
            state.user.data = data
        },
        setSortType(state, sortType) {
            state.sortType = sortType
        },
        setCurrentBot(state, bot) {
            state.currentBot = bot
        },
        setCurrentFilter(state, filter) {
            state.currentFilter = filter
        },
        setHiddenStatsIndexes(state, hiddenStatsIndexes) {
            state.hiddenStatsIndexes = hiddenStatsIndexes
        }
    },
    actions: {
        fetchUser({ commit }, user) {
            commit('setLoggedIn', user !== null)
            if (user) {
                commit('setUser', {
                    displayName: user.displayName,
                    email: user.email
                })
            } else {
                commit('setUser', null)
            }
        },
        setHiddenStatsIndexes({ commit }, hiddenStatsIndexes) {
            commit('setHiddenStatsIndexes', hiddenStatsIndexes)
        },
        bindFirebaseCollections: firebaseAction(({ state, bindFirebaseRef }, collections) => {
            for (const collection of collections) {
                bindFirebaseRef(collection, firebase.database().ref(collection))
            }
        }),
        unbindFirebaseCollections: firebaseAction(({ state, unbindFirebaseRef }, collections) => {
            for (const collection of collections) {
                unbindFirebaseRef(collection, firebase.database().ref(collection))
            }
        }),
        bindFirebaseCurrentBot: firebaseAction(({ state, bindFirebaseRef }, bot) => {
            if (bot.id === undefined) return
            bindFirebaseRef('currentBot', firebase.database().ref('bots').child(bot.id))
        }),
        unbindFirebaseCurrentBot: firebaseAction(({ state, unbindFirebaseRef }) => {
            if (!state.currentBot) return
            const currentBotId = state.currentBot['.key']
            if (currentBotId === undefined) return
            unbindFirebaseRef('currentBot', firebase.database().ref('bots').child(currentBotId))
        }),
        bindFirebaseCurrentFilter: firebaseAction(({ state, bindFirebaseRef }, filter) => {
            if (filter.id === undefined) return
            bindFirebaseRef('currentFilter', firebase.database().ref('filters').child(filter.id))
        }),
        unbindFirebaseCurrentFilter: firebaseAction(({ state, unbindFirebaseRef }) => {
            if (!state.currentFilter) return
            const currentFilterId = state.currentFilter['.key']
            if (currentFilterId === undefined) return
            unbindFirebaseRef('currentFilter', firebase.database().ref('filters').child(currentFilterId))
        })
    }
})
