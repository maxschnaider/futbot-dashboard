import firebase from '../services/firebase'
import store from '../store'
import datetimeHelper from '../helpers/datetimeHelper'
// import { Message } from '../models/Chat'

export default class Bot {
    constructor(data) {
        Object.defineProperty(this, 'id', {
            value: parseInt(typeof data === "object" && data !== null ? data['.key'] : 0),
            writable: true
        })
        data = typeof data === "object" && data !== null ? data : {}
        let _filterId = data.filter_id
        Object.defineProperty(this, "filterId", {
            set: function(v) {
                _filterId = v
                this.update('filter_id', v)
            },
            get: function() {
                return _filterId || ''
            }
        })
        let _name = data.name
        Object.defineProperty(this, "name", {
            set: function(v) {
                _name = v
                this.update('name', v)
            },
            get: function() {
                return _name || ''
            }
        })
        this.balance = data.balance !== undefined ? data.balance : 0
        this.lastTradeTimestamp = data.last_trade_timestamp
        this.lastTradeDate = this.lastTradeTimestamp !== undefined ? Date.parse(this.lastTradeTimestamp) : null
        this.minutesSinceLastTrade = datetimeHelper.minutesSinceDate(this.lastTradeDate)

        // stats
        this.statsTypes = ['trades', 'balance', 'fails']
        this.statsByHour = {}

        // helpers
        this.dateToTime = datetimeHelper.dateToTime
        this.minutesSinceDate = datetimeHelper.minutesSinceDate
        this.ISOTimestampToLocalTima = datetimeHelper.ISOTimestampToLocalTima
        this.todayDateStr = datetimeHelper.todayDateStr
        this.utcISOStringForHour = datetimeHelper.utcISOStringForHour
    }

    toJSON() {
        return {
            'filter_id': this.filterId,
            // 'config': this.config
        }
    }

    // async fetchTodayTrades() {
    //     let snapshot, trades
    //     snapshot = await firebase.database().ref(`stats/bots/${this.id}/trades/${this.todayDateStr()}`).once("value")
    //     this.trades = snapshot.val()
    //     if (!(trades instanceof Object)) return
    //     return trades
    // }

    async fetchTodayStats() {
        this.statsTypes = ['trades', 'fails', '404', 'stops', 'balance', 'profit']
        this.statsByHour = {}
        for (let hour = 0; hour < 24; hour++) {
            this.statsByHour[`${this.utcISOStringForHour(hour)}`] = {
                'trades': [],
                'fails': [],
                '404': [],
                'stops': [],
                'balance': [],
                'profit': 0
            }
        }
        const dbStatsTypes = ['trades', 'fails', 'balance']
        for (let dbStatsType of dbStatsTypes) {
            const snapshot = await firebase.database().ref(`stats/bots/${this.id}/${dbStatsType}/${this.todayDateStr()}`).once("value")
            const data = snapshot.val()
            if (!data) return
            for (let statsItem of data) {
                if (statsItem === undefined || statsItem.timestamp === undefined) continue
                Object.keys(this.statsByHour).map(hourISOString => {
                    if (hourISOString.includes(statsItem.timestamp.substring(0, 13))) {
                        let statsType = dbStatsType
                        if (dbStatsType === 'fails') {
                            if (statsItem.type === 'pitstop') {
                                statsType = 'stops'
                            } else if (statsItem.type === 'search') {
                                statsType = '404'
                            }
                        }
                        this.statsByHour[hourISOString][statsType].push(statsItem)
                    }
                })
            }
        }
        Object.keys(this.statsByHour).map(hourISOString => {
            const hourlyBalances = this.statsByHour[hourISOString].balance
            if (hourlyBalances.length > 1) {
                const profit = hourlyBalances[hourlyBalances.length - 1].balance - hourlyBalances[0].balance
                this.statsByHour[hourISOString].profit = profit > 0 && profit < 50000 ? profit : 0
            }
        })
    }

    statsToSeries() {
        let series = []
        for (let idx in this.statsTypes) {
            let statsType = this.statsTypes[idx]
            series.push({
                name: statsType,
                data: [],
                type: statsType === 'balance' ? 'area' : 'bar'
            })
            for (let hour = 0; hour < 24; hour++) {
                let data = 0
                const timestamp = this.utcISOStringForHour(hour)
                if (this.statsByHour[timestamp] === undefined || this.statsByHour[timestamp][statsType] === undefined) continue
                if (statsType === 'profit') {
                    data = this.statsByHour[timestamp]['profit']
                } else {
                    for (let statsItem of this.statsByHour[timestamp][statsType]) {
                        if (statsType === 'balance') {
                            data = statsItem.balance
                        } else {
                            data += 1
                        }
                    }
                }
                series[idx].data.push(data)
            }
        }
        return series
    }

    update(key, value) {
        if (this.id && value !== undefined)
            return firebase.database().ref(`bots/${this.id}/${key}`).set(value)
    }

    save() {
        if (this.id)
            return firebase.database().ref(`bots/${this.id}`).update(this.toJSON())
    }

    remove() {
        if (this.id)
            return firebase.database().ref(`bots/${this.id}`).set(null)
    }
}
