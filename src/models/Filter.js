import firebase from '../services/firebase'
import store from '../store'
import datetimeHelper from '../helpers/datetimeHelper'

export default class Filter {
    constructor(data) {
        Object.defineProperty(this, 'id', {
            value: parseInt(typeof data === "object" && data !== null ? data['.key'] : 0),
            writable: true
        })
        data = typeof data === "object" && data !== null ? data : {}
        this.lastPricing = data.last_pricing
        let _club = data.club
        Object.defineProperty(this, "club", {
            set: function(v) {
                _club = v
                this.update('club', v)
            },
            get: function() {
                return _club || ''
            }
        })
        let _league = data.league
        Object.defineProperty(this, "league", {
            set: function(v) {
                _league = v
                this.update('league', v)
            },
            get: function() {
                return _league || ''
            }
        })
        let _maxBuyNow = data.max_buy_now
        Object.defineProperty(this, "maxBuyNow", {
            set: function(v) {
                _maxBuyNow = v
                this.update('max_buy_now', v)
            },
            get: function() {
                return _maxBuyNow || ''
            }
        })
        let _minBuyNow = data.min_buy_now
        Object.defineProperty(this, "minBuyNow", {
            set: function(v) {
                _minBuyNow = v
                this.update('min_buy_now', v)
            },
            get: function() {
                return _minBuyNow || ''
            }
        })
        let _minPrice = data.min_price
        Object.defineProperty(this, "minPrice", {
            set: function(v) {
                _minPrice = v
                this.update('min_price', v)
            },
            get: function() {
                return _minPrice || ''
            }
        })
        let _name = data.name
        Object.defineProperty(this, "name", {
            set: function(v) {
                _name = v
                this.update('name', v)
            },
            get: function() {
                return _name || 'ноунэйм'
            }
        })
        let _playerName = data.player_name
        Object.defineProperty(this, "playerName", {
            set: function(v) {
                _playerName = v
                this.update('player_name', v)
            },
            get: function() {
                return _playerName || ''
            }
        })
        let _position = data.position
        Object.defineProperty(this, "position", {
            set: function(v) {
                _position = v
                this.update('position', v)
            },
            get: function() {
                return _position || ''
            }
        })
        let _quality = data.quality
        Object.defineProperty(this, "quality", {
            set: function(v) {
                _quality = v
                this.update('quality', v)
            },
            get: function() {
                return _quality || ''
            }
        })
        let _region = data.region
        Object.defineProperty(this, "region", {
            set: function(v) {
                _region = v
                this.update('region', v)
            },
            get: function() {
                return _region || ''
            }
        })
        let _sellPrice = data.sell_price
        Object.defineProperty(this, "sellPrice", {
            set: function(v) {
                _sellPrice = v
                this.update('sell_price', v)
            },
            get: function() {
                return _sellPrice || ''
            }
        })
        Object.defineProperty(this, "profit", {
            value: data.sell_price && data.max_buy_now ? data.sell_price * 0.95 - data.max_buy_now : 0,
            writable: false
        })

        // helper
        this.ISOTimestampToLocalTima = datetimeHelper.ISOTimestampToLocalTima
    }

    toJSON() {
        return {
            "club" : this.club ,
            "league" : this.league ,
            "max_buy_now" : this.maxBuyNow ,
            "min_buy_now" : this.minBuyNow ,
            "min_price" : this.minPrice ,
            "name" : this.name ,
            "player_name" : this.playerName ,
            "position" : this.position ,
            "quality" : this.quality ,
            "region" : this.region ,
            "sell_price" : this.sellPrice
        }
    }

    update(key, value) {
        if (this.id && value !== undefined)
            return firebase.database().ref(`filters/${this.id}/${key}`).set(value)
    }

    save() {
        if (this.id)
            return firebase.database().ref(`filters/${this.id}`).update(this.toJSON())
    }

    remove() {
        if (this.id)
            return firebase.database().ref(`filters/${this.id}`).set(null)
    }
}
