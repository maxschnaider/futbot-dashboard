import Vue from 'vue'
import Router from 'vue-router'
import store from '../store'
import firebase from "../services/firebase"

import Login from '../pages/Login.vue'
import Dashboard from '../pages/Dashboard.vue'

Vue.use(Router)

const router = new Router({
    mode: 'history',
    base: process.env.BASE_URL,
    routes: [
        {
            path: '/login',
            name: 'Login',
            component: Login
        },
        {
            path: '*',
            name: 'Dashboard',
            component: Dashboard,
            meta: {
                requiresAuth: true
            }
        }
    ]
})

router.beforeEach(async (to, from, next) => {
    let currentUser
    if (!store.state.user.loggedIn || !store.state.user.data) {
        currentUser = await firebase.auth().currentUser
    } else {
        currentUser = true
    }

    if (to.matched.some(record => record.meta.requiresAuth)) {
        if (!currentUser) {
            next({
                path: '/login',
                query: { redirect: to.fullPath }
            })
        } else {
            next()
        }
    } else {
        if (to.path === '/login' && currentUser) {
            next({ name: 'Dashboard' })
        }
        next()
    }
})

export default router
