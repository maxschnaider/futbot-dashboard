import * as firebase from "firebase"
// import { initializeAppCheck, ReCaptchaV3Provider } from "@firebase/app-check"

const configOptions = {
    apiKey: "AIzaSyAPFdXA_uExj44NhG7t8SfumKnBvcFGz4c",
    authDomain: "futbot-ai.firebaseapp.com",
    // databaseURL: "https://futbot-ai.firebaseio.com",
    databaseURL: "https://futbot-ai-default-rtdb.europe-west1.firebasedatabase.app",
    projectId: "futbot-ai",
    storageBucket: "futbot-ai.appspot.com",
    messagingSenderId: "789931359448",
    appId: "1:789931359448:web:3d772e284a491d114febad"
}

const app = firebase.initializeApp(configOptions)
// const appCheck = initializeAppCheck(app, {
//     provider: new ReCaptchaV3Provider('6LfLxG4eAAAAAIwkfiNntm-sAdCRRS25RvbVqUrP\n'),
//     isTokenAutoRefreshEnabled: true
// })

export default app
