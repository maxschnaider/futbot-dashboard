import Vue from 'vue'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import {  } from '@fortawesome/free-solid-svg-icons'
import {  } from '@fortawesome/free-regular-svg-icons'
import { faSnapchatGhost, faPlaystation } from '@fortawesome/free-brands-svg-icons'

library.add(faSnapchatGhost, faPlaystation)

Vue.component('font-awesome-icon', FontAwesomeIcon)
